Rails.application.routes.draw do
  resources :tours, except: [:new, :edit]
  resources :tour_guides, except: [:new, :edit]
  resources :tenants, except: [:new, :edit]
  resources :stays, except: [:new, :edit]
  resources :properties, except: [:new, :edit]
  resources :companies, except: [:new, :edit]
end
