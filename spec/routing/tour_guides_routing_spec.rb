require "rails_helper"

RSpec.describe TourGuidesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/tour_guides").to route_to("tour_guides#index")
    end

    it "routes to #new" do
      expect(:get => "/tour_guides/new").to route_to("tour_guides#new")
    end

    it "routes to #show" do
      expect(:get => "/tour_guides/1").to route_to("tour_guides#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/tour_guides/1/edit").to route_to("tour_guides#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/tour_guides").to route_to("tour_guides#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/tour_guides/1").to route_to("tour_guides#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/tour_guides/1").to route_to("tour_guides#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/tour_guides/1").to route_to("tour_guides#destroy", :id => "1")
    end

  end
end
