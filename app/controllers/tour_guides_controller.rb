# @class TourGuidesController
# @author Patrick
class TourGuidesController < ApplicationController
  def index
    @tour_guides = TourGuide.all
  end

  def show
    @tour_guide = TourGuide.find(params[:id])
  end

  def create
    @tour_guide = TourGuide.new(tg_params)
    return render json: @tour_guide,
                  status: :created,
                  location: @tour_guide if @tour_guide.save
    render json: @tour_guide.errors, status: :unprocessable_entity
  end

  private

  def tg_params
    params.require(:tour_guide).permit(:name, :email, :phone)
  end
end
