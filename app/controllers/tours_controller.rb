# @class ToursController
# @author Patrick
class ToursController < ApplicationController
  def index
    @tours = Tour.where(nil)
    @tours = @tours.matches_tour_guide(
      params[:tour_guide]
    ) if params[:tour_guide].present?
    @tours = @tours.matches_property(
      params[:property]
    ) if params[:property].present?
    # allowing two optional filters, this can be abstracted
    # if desired by just naming the scopes the param name
    # and looping the total params on the GET request
  end

  def show
    @tour = Tour.find(params[:id])
  end

  def create
    # create tour object and assign a tour guide
    @tour = Tour.new(t_params)

    # now we can call special tour_guide assignment logic here
    @tour.assign_tour_guides

    return render json: @tour, status: :created, location: @tour if @tour.save
    render json: @tour.errors, status: :unprocessable_entity
  end

  def update
    # Change the status for a tour (potential cancelation)
    @tour = Tour.find(params[:id])

    return head :no_content if @tour.update(t_params)
    render json: @tour.errors, status: :unprocessable_entity
  end

  private

  def t_params
    params.require(:tour).permit(:attendee_name,
                                 :attendee_phone, :attendee_email, :tour_at,
                                 :tour_guide_id, :property_id, :status_flag)
    # probably eventually want to only permit changing of attributes
    # that we want from the front end in this case, only status_flag
  end
end
