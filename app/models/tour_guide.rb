# @class TourGuide
# @author Patrick
#
class TourGuide < ActiveRecord::Base
  has_many :tours
end
