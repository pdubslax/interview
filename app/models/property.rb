# @class Company
# @author Evan
#
# Property models represents a physical building
#
class Property < ActiveRecord::Base
  belongs_to :company

  has_many :stays

  has_many :tours

  def current_stays
    stays.on_date(Date.today)
  end
end
