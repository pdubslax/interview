# @class Tour
# @author Patrick
#
class Tour < ActiveRecord::Base
  belongs_to :tour_guide
  belongs_to :property
  after_save :send_email

  enum statusFlagMapping: { unscheduled: 0,
                            scheduled: 1,
                            canceled: 2 }

  scope :matches_tour_guide, lambda { |tour_guide_id|
    where 'tour_guide_id == ?', tour_guide_id
  }
  scope :matches_property, lambda { |property_id|
    where 'property_id == ?', property_id
  }

  def assign_tour_guide
    self.status_flag = Tour.statusFlagMappings[:scheduled]
    # can put more complex assignment logic here
  end

  def cancel_tour
    self.status_flag = Tour.statusFlagMappings[:canceled]
  end

  private

  def send_email
    # email contents can depend on the state of the tour
    # read status and pass different param to
    # the email sending service depending
    message = ""
    case :status_flag
    when Tour.statusFlagMappings[:scheduled]
      message = 'Your tour has been scheduled!'
    when Tour.statusFlagMappings[:canceled]
      message = 'Your tour has been canceled :('
    when Tour.statusFlagMappings[:unscheduled]
      return
    end
    # just using the hard coded strings as an example but
    # ideally could pass a param to the email module

    prop_id = :property_id
    tour_date = :tour_at
    tour_guide_id = :tour_guide_id

    list_of_stays_affected = Stay.on_date(tour_date).for_property(prop_id).all
    list_of_stays_affected.each { |stay|
      stay_id = stay.id
      list_of_tenants_affected_by_stay = Tenant.for_stay(stay_id)
      list_of_tenants_affected_by_stay.each { |tenant|
        tenant_email = tenant.email
        # send the email alerting tenant about upcoming tour
      }
    }

    tour_guide_email = TourGuide.find(tour_guide_id).email
    # send a confirmation email to the tour guide

    tour_attendee_email = :attendee_email
    # send a confirmation email to the tour attendee
    # could also put all the reciepients in a single array
    # above here to get them all in the same email thread
  end
end
