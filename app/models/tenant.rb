# @class Tenant
# @author Evan
#
# Tenant model represents a person living in a house. To make things simpler,
# we can assume that tenants are only the people staying in the house and
# they are not going on tours.
#
class Tenant < ActiveRecord::Base
  belongs_to :stay

  scope :stay_equal, -> (stayID) {where('stay_id == ?',stayID) }

  def self.for_stay(stayID)
  	stay_equal(stayID)
  end

end
