class CreateTenants < ActiveRecord::Migration
  def change
    create_table :tenants do |t|
      t.string :name
      t.string :email
      t.references :stay, index: true

      t.timestamps null: false
    end
    add_foreign_key :tenants, :stays
  end
end
