class CreateTours < ActiveRecord::Migration
  def change
    create_table :tours do |t|
      t.string :attendee_name
      t.string :attendee_phone
      t.string :attendee_email
      t.datetime :tour_at
      t.integer :status_flag

      t.references :tour_guide, index: true
      t.references :property, index: true

      t.timestamps null: false
    end
    add_foreign_key :tours, :tour_guide
    add_foreign_key :tours, :properties
  end
end
