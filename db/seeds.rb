c = Company.create(name: 'Test Company',
                   phone: '1234567890')

p1 = Property.create(street_address: '1052 Baldwin',
                    beds: 10,
                    baths: 4.5,
                    rent: 990_000,
                    company: c)

p2 = Property.create(street_address: '1052 Baldwin',
                    beds: 10,
                    baths: 4.5,
                    rent: 990_000,
                    company: c)

s = Stay.create(start_date: 1.month.ago,
                end_date: 10.months.from_now,
                property: p1)

Tenant.create(name: 'Tim Jones',
              email: 'tim@cribspot.com',
              stay: s)

tg1 = TourGuide.create(name:'Patrick', 
                       phone:'2133123123',
                       email: 'tourguideRus@co.uk')

tg2 = TourGuide.create(name:'Dan', 
                       phone:'25435123123',
                       email: 'dan@co.uk')

tg3 = TourGuide.create(name:'Rose', 
                       phone:'94509234',
                       email: 'rose@co.uk')

t1 = Tour.create(attendee_name: 'Dan',
                 attendee_phone: '324324324',
                 attendee_email: 'dfgdf@sdfs.com',
                 tour_at: 2.weeks.from_now,
                 tour_guide: tg1,
                 property: p1,
                 status_flag: 1)

t2 = Tour.create(attendee_name: 'Dan',
                 attendee_phone: '324324324',
                 attendee_email: 'dfgdf@sdfs.com',
                 tour_at: 1.weeks.from_now,
                 tour_guide: tg2,
                 property: p2,
                 status_flag: 1)

t3 = Tour.create(attendee_name: 'Dan',
                 attendee_phone: '324324324',
                 attendee_email: 'dfgdf@sdfs.com',
                 tour_at: 3.weeks.from_now,
                 tour_guide: tg3,
                 property: p2,
                 status_flag: 1)
